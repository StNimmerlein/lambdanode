export const greet = async ({firstName, lastName, age}) => {
    if (age < 18) {
        return `Hi ${firstName}`
    }
    return `Hello Mr/Mrs ${lastName}`
}