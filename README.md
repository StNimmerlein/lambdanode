# How to use

Run `yarn build`. Then create a zip file with the contents from `dist` and upload it as Lambda with handler `greeter.greet` to AWS.